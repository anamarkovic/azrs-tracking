# azrs-tracking

Projekat u okviru kursa Alati za razvoj softvera za praćenje napretka primene alata na projektu  [RS022-tebrafile](https://gitlab.com/anamarkovic/RS022-tebrafile), koji je razvijan u okviru kursa Razvoj softvera.

# Primenjeni alati:

1. [Git](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/13)
2. [CI](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/12)
3. [Docker](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/6)
4. [GCov](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/9)
5. [Meld](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/14)
6. [CMake](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/1)
7. [Doxygen](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/11)
8. [Clang Analyzer](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/10)
9. [Callgrind](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/8)
10. [Valgrind](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/7)
11. [Git Hooks](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/4)
12. [CLang Format](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/3)
13. [Clang Tidy](https://gitlab.com/anamarkovic/azrs-tracking/-/issues/2)
